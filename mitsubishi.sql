-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 10.1.19-MariaDB - mariadb.org binary distribution
-- OS Server:                    Win32
-- HeidiSQL Versi:               9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Membuang struktur basisdata untuk mitsubishi
CREATE DATABASE IF NOT EXISTS `mitsubishi` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mitsubishi`;

-- membuang struktur untuk table mitsubishi.article
CREATE TABLE IF NOT EXISTS `article` (
  `id_article` int(11) NOT NULL AUTO_INCREMENT,
  `judul_article` varchar(50) DEFAULT NULL,
  `isi_article` text,
  `image` text,
  PRIMARY KEY (`id_article`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel mitsubishi.article: ~2 rows (lebih kurang)
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` (`id_article`, `judul_article`, `isi_article`, `image`) VALUES
	(1, 'Otomotif Ku', 'Sign up for CNN\'s new US politics newsletter, Meanwhile in America', '201910000802asdasdas.jpg'),
	(2, 'Otomotif berita terbaru', 'Kurt Cobain\'s ex-Seattle home is for sale', '201910500802as.jpg'),
	(3, 'Otomotif berita terbaru', 'Biggest superyacht fleet ever invades Monaco', '201910070802190925110715-wolf-warrior-movie-large-tease.jpg');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;

-- membuang struktur untuk table mitsubishi.banner
CREATE TABLE IF NOT EXISTS `banner` (
  `id_banner` int(11) NOT NULL AUTO_INCREMENT,
  `nama_banner` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id_banner`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel mitsubishi.banner: ~2 rows (lebih kurang)
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` (`id_banner`, `nama_banner`, `deskripsi`, `image`) VALUES
	(27, 'Outlander', '-', '201910200604Outlander.jpg'),
	(28, 'Pajero', '-', '201910000604Pajero.jpg');
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;

-- membuang struktur untuk table mitsubishi.kategori
CREATE TABLE IF NOT EXISTS `kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(50) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel mitsubishi.kategori: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
	(2, 'Mobil xpander');
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;

-- membuang struktur untuk table mitsubishi.produk
CREATE TABLE IF NOT EXISTS `produk` (
  `id_produk` int(11) NOT NULL AUTO_INCREMENT,
  `nama_produk` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` varchar(50) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `banyak_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id_produk`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel mitsubishi.produk: ~3 rows (lebih kurang)
/*!40000 ALTER TABLE `produk` DISABLE KEYS */;
INSERT INTO `produk` (`id_produk`, `nama_produk`, `deskripsi`, `harga`, `id_kategori`, `image`, `banyak_type`) VALUES
	(1, 'Mobil X pander', 'Inovasi sistem plugin in ev Hybrid sangat eco- efficient. pada mode all electric driving , anda akan merasakan berkendara bebas konsumsi bahan bakar', '200.000.000', 2, '201910310904Pajero-Dakar.jpg', '2'),
	(2, 'Mitsubishi Delica', 'mobil yang didesain untuk keluarga anda', '150.000.000', 2, '201910220604Mitsubishi-Delica.jpg', '3'),
	(3, 'Mitsubishi Outlander', 'Di desain untuk anda yang ingin kepuasan dalam berkendara', '230.000.000', 2, '201910190604Mitsubishi-Outlander-Sport.jpg', '45');
/*!40000 ALTER TABLE `produk` ENABLE KEYS */;

-- membuang struktur untuk table mitsubishi.testimony
CREATE TABLE IF NOT EXISTS `testimony` (
  `id_testimony` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(200) NOT NULL DEFAULT '0',
  `mobil` varchar(200) NOT NULL DEFAULT '0',
  `nama_user` varchar(50) DEFAULT NULL,
  `foto_profile` varchar(50) DEFAULT NULL,
  `pesan` text,
  PRIMARY KEY (`id_testimony`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel mitsubishi.testimony: ~3 rows (lebih kurang)
/*!40000 ALTER TABLE `testimony` DISABLE KEYS */;
INSERT INTO `testimony` (`id_testimony`, `image`, `mobil`, `nama_user`, `foto_profile`, `pesan`) VALUES
	(1, '201910270602as.jpg', 'mitsubishi xpander', 'Fanny Aulia', NULL, 'Marketing Sangat Rekomeded pokonya pelayanan nya bagus '),
	(2, '201910400602download.jpg', 'Mitsubishi Terios', 'Gita Putri', NULL, 'Beli mobil yang sama yang berpengalaman dong, jangan ngasal loh y , saya rekomendasi teteh ini'),
	(3, '20191053060225176935_0654a1d8-7c24-4123-a069-a72757dc2e71_525_700.jpg', 'Mobil Xmach', 'Dina Septiana', NULL, 'Saya rekomendasi kan marketing paling cantik ');
/*!40000 ALTER TABLE `testimony` ENABLE KEYS */;

-- membuang struktur untuk table mitsubishi.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel mitsubishi.users: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`) VALUES
	(1, 'wildan.sutisna', 'ac174e67e662c4d31dcc2e8b16358024');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
