<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_controllers extends CI_Controller {
   

    public function __construct() {
        parent::__construct();
        $this->load->model("model_crud");
  }
  


  public function tambah() {
        $table=$_POST["nama_table"];

        $data=$_POST["inputan"];

      
        if(!empty($_FILES)){
          if(file_exists(realpath(APPPATH . '../uploads/'.$_FILES["uploads"]["name"]))){
            $this->load->helper("file");
            unlink(realpath(APPPATH . '../uploads/'.$_FILES["uploads"]["name"]));
        
          }
         $upload= $this->aksi_upload($_POST["nama_table"],$_FILES["uploads"]);
          if($upload){
            $data["image"]=$upload."".$_FILES["uploads"]["name"];
          }
        }

        $simpan=$this->model_crud->simpandata($table,$data);   
        if($simpan){
            $this->session->set_flashdata('msg', 'Simpan Berhasil');
          }else{
            $this->session->set_flashdata('msg', 'Gagal Simpan');
          }
         redirect($_POST["base_url"]);
         
    }
   

    public function edit() {
        $userid["".$_POST["kolom_id"]] = $_POST["id"];
        $table=$_POST["nama_table"];
        $data=$_POST["inputan"];
        if(!empty($_FILES)){
          $dataedit=$this->model_crud->getdata($table,"",$userid);
          foreach($dataedit as $d){
            if(file_exists(realpath(APPPATH . '../uploads/'.$d->image))){
              $this->load->helper("file");
              unlink(realpath(APPPATH . '../uploads/'.$d->image));
            }
          }

          if(!empty($_FILES["uploads"]["name"])){
              $upload= $this->aksi_upload($_POST["nama_table"],$_FILES["uploads"]);
            if($upload){

              $data["image"]=$upload."".$_FILES["uploads"]["name"];
            }     
          }
        }
 

        $update=$this->model_crud->updatedata($table,$data,$userid); 
        if($update){
            $this->session->set_flashdata('msg', 'Simpan Berhasil');

          }else{
            $this->session->set_flashdata('msg', 'Gagal Simpan');
          }

                    redirect($_POST["base_url"]);
    }
    
    public function hapus() {
        $deletedata[''.$_POST["kolom_id"]] = $_POST["id"];  
        
        $table=$_POST["table"];
        $data=$this->model_crud->getdata($table,"",$deletedata);
        $delete=$this->model_crud->deletedata($table,$deletedata);
        if($delete){
          $this->session->set_flashdata('msg', 'Berhasil Di Hapus');
          foreach($data as $d){
            if(file_exists(realpath(APPPATH . '../uploads/'.$d->image))){
              $this->load->helper("file");
              unlink(realpath(APPPATH . '../uploads/'.$d->image));
            }
        }
        
        }else{
          $this->session->set_flashdata('msg', 'Gagal Menghapus Data');
        }
        echo json_encode(base("backend/".$table));
    }

   
    public function aksi_upload($nama,$file){
      $namefile="".date('Y')."".date('m')."".date("s")."".date("h")."".date("d");
      $image_path = realpath(APPPATH . '../uploads');
      $config['upload_path']          = $image_path;
      $config['allowed_types']        = 'jpg|png';
      $config['file_name'] =$namefile."".$_FILES["uploads"]["name"];
      $this->load->library('upload', $config);
      if ( ! $this->upload->do_upload("uploads")){
        return false;

      }else{
        return $namefile;
      }
    }
}
