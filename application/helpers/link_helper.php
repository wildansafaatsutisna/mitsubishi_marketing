<?php
function backend_asset_css($url) {
    return base_url()."assets/tampilan/backend/css/".$url;
}
function backend_asset_js($url) {
    return base_url()."assets/tampilan/backend/js/".$url;
}
function backend_asset_font($url) {
    return base_url()."assets/tampilan/backend/fonts/".$url;
}
function backend_asset_images($url) {
    return base_url()."assets/tampilan/backend/images/".$url;
}
function backend_asset_img($url) {
    return base_url()."assets/tampilan/backend/img/".$url;
}
function backend_asset_vendor($url) {
    return base_url()."assets/vendor/".$url;
}
function base($url) {
    return base_url()."".$url;
}
function base_masterdata($url) {
    return base_url()."backend/".$url;
}


function uploadan($url) {
    return base_url()."uploads/".$url;
}

function frontend_assets($url) {
    return base_url()."assets/tampilan/frontend/".$url;
}






?>