<?php



function template_breadcump($title,$deskripsi,$link2) {
    $html='
    <div class="breadcomb-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcomb-list">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="breadcomb-wp">
                                    <div class="breadcomb-icon">
                                        <i class="notika-icon notika-windows"></i>
                                    </div>
                                    <div class="breadcomb-ctn">
                                        <h2>'.$title.'</h2>
                                        <p>'.$deskripsi.'</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    ';
    return $html;
}


function template_card_body($html_table){
    $html='
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
            '.$html_table.'
            </div>
        </div>
      </div>
    </section>
    ';
    return $html;
}








function template_data_table($table,$id,$data,$kolom,$action,$base,$to,$buttoncustom=""){
    $html='
    <div class="data-table-area">
    <div class="container">
        <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                '.tambah_action($action,$table,$base,$to).'

                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped">
                            <thead>
                                <tr>
                                   '.header_table($kolom,$action,$table,$base,$to).'
                                </tr>
                            </thead>
                            <tbody>
                            '.isi_data_table($data,$kolom,$action,$table,$base,$id,$to,$buttoncustom).'                                    
                            </tbody>
                           
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    ';


    return $html;
}



function header_table($header,$action,$table,$base,$to){
    $html="";
    $edit_action=edit_action($action,$table,$base,'','').''.hapus_action($action,$table,$base,'','',$to);
    foreach($header as $head){
        
        $html .="<th>".$head['label']."</th>";
    }
    if($edit_action!=""){
        $html .="<th>Action</th>";
    }
    return $html;
}

function isi_data_table($data,$kolom,$action,$table,$base,$id,$to,$buttoncustom=""){
    $html="";
    foreach ($data as $key) {
       $edit_action=edit_action($action,$table,$base,$key->$id,$to);
       $hapus_action=hapus_action($action,$table,$base,$key->$id,$id,$to);
       $button_customs="";
       if(!empty($buttoncustom)){
       $button_customs=custom_action($buttoncustom,$key->$id);
       }

       $html .="<tr>";
                foreach($kolom as $kol){
                    if($kol["jenis"]=="image"){
                        $html .="<td><img width='100px' src='".uploadan($key->$kol["view"])."' class='img-fluid'></td>";
                    }else{
                        $html .="<td>".$key->$kol["view"]."</td>";

                    }        
                }
   
    if(($edit_action!="")||($hapus_action!="")){
           $html .="<td>".$button_customs."".$edit_action."".$hapus_action."</td>";
       }

       $html .="</tr>";
    }
    return $html;    
}



function tambah_action($action,$table,$base,$to){
    $html="";
    foreach ($action as $d) {
        if($d=="tambah"){
            $html .='
            <div class="breadcomb-report">
            <a href='.$base.''.$to."/".$d.'_'.$to.'  data-toggle="tooltip" data-placement="left" title="Tambah Data" class="btn">Tambah Data</a>
            </div>
            ';
        }
    }
    return $html;

}


function edit_action($action,$table,$base,$id,$to){
    $html="";
    foreach ($action as $d) {
        if($d=="edit"){
            $html .='
            <a href='.$base.''.$to."/".$d.'_'.$to.'/'.$id.' data-toggle="tooltip" data-placement="left" title="Edit Data" class="btn btn-sm ">Edit</a>
            ';
        }
    }
    return $html;
}

function custom_action($buttoncustom,$id){
    $html="";

    foreach($buttoncustom as $btn){
        $html .='<a href="'.$btn[2].'/'.$id.'" class="btn '.$btn[5].'">'.$btn[4].'</a>';
    }
      return $html;
}

function hapus_action($action,$table,$base,$id,$kolom_id,$to){

    $html="";
    foreach ($action as $d) {
        if($d=="hapus"){
            $html .='
            <button data-kolom_id='.$kolom_id.' data-id='.$id.' data-table='.$to.'  data-toggle="tooltip" data-placement="left" title="Hapus Data" class="btn btn-sm btn-danger btn_hapus ">Hapus</button>
            ';
        }
    }
    return $html;
}




?>