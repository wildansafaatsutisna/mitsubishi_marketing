<?php
function template_edit_form($table,$kolom,$base,$data_item,$kolom_id){

    $html="";
    $html .='<form enctype="multipart/form-data" data-parsley-validate="" novalidate="" method="post" action="'.base_url().'crud_controllers/edit">';
    $html .='<input type="hidden" name="nama_table" value="'.$table.'">';
    $html .='<input type="hidden" name="base_url" value="'.$base.'">';
    $html .='<input type="hidden" name="id" value="'.$data_item[0]->$kolom_id.'">';
    $html .='<input type="hidden" name="kolom_id" value="'.$kolom_id.'">';
    $html .="".header_form();
        foreach($kolom as $kol => $data){
            if($data["jenis"]=="input"){
                $html .="".input_form_edit($kol,$data,$data_item[0]);                
            }else if($data["jenis"]=="select"){
                $html .="".select_form_edit($kol,$data,$data["isi"],$data_item[0]);                
            }else if($data["jenis"]=="file"){
                $html .="".upload_form_edit($kol,$data,$data_item[0],$data_item[0]);
            }else if($data["jenis"]=="textarea"){
                $html .="".textarea_form_edit($kol,$data,$data_item[0]);
            }
        }
        $html .="".submit_form();
    $html .="".footer_form();
    $html .='</form>';
    return $html;

}



function input_form_edit($name,$jenis,$data_item){
    $html='
    <div class="form-example-int">
        <div class="form-group">
            <label>'.$jenis["label"].'</label>
            <div class="nk-int-st">
                <input  required="" type="text" name="inputan['.$name.']" value="'.$data_item->$name.'" class="form-control input-sm" placeholder="'.$jenis["placeholder"].'">
            </div>
        </div>
    </div>';
    return $html;
}


function upload_form_edit($name,$jenis,$data_item){
    $html_image="";
    if($data_item->$name!=""){
        $html_image='<img width="100px" src="'.uploadan($data_item->$name).'">';
    }
    $html='
    <div class="form-example-int">
        <div class="form-group">
            <label>'.$jenis["label"].'</label>
            <div class="nk-int-st">
            '.$html_image.'

            <input  type="file" name="uploads" />
            </div>
        </div>
    </div>';
    return $html;
}


function textarea_form_edit($name,$jenis,$data_item){
    $html='
    <div class="form-example-int">
        <div class="form-group">
            <label>'.$jenis["label"].'</label>
            <div class="nk-int-st">
                <textarea  required=""  name="inputan['.$name.']"  class="form-control input-sm" placeholder="'.$jenis["placeholder"].'">'.$data_item->$name.'</textarea>
            </div>
        </div>
    </div>';
    return $html;
}

function select_form_edit($name,$jenis,$array,$data_item){

    $option="";
    foreach($array as $a){
        if($data_item->$jenis["view"]==$a->$jenis["view"]){
             $option .='<option selected value='.$a->$jenis["id"].'>'.$a->$jenis["value"].'</option>';
        }else{
            $option .='<option value='.$a->$jenis["id"].'>'.$a->$jenis["value"].'</option>';

        }
    }

    $html='
    <div class="form-example-int">
        <div class="form-group">
            <label>'.$jenis["label"].'</label>
            <div class="nk-int-st">
                <select required="" class="form-control" name="inputan['.$name.']">
                <option value="">Silahkan Pilih</option>
                '.$option.'
                </select>
            </div>
        </div>
    </div>';
    return $html;
}



?>