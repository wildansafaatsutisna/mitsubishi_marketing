<?php
function template_tambah_form($table,$kolom,$base){
    
    $html="";
    $html .='<form enctype="multipart/form-data"  data-parsley-validate="" novalidate="" method="post" action="'.base_url().'crud_controllers/tambah">';
    $html .='<input type="hidden" name="nama_table" value="'.$table.'">';
    $html .='<input type="hidden" name="base_url" value="'.$base.'">';
    $html .="".header_form();

    foreach($kolom as $kol => $data){
            if($data["jenis"]=="input"){
                $html .="".input_form($kol,$data);                
            }else if($data["jenis"]=="select"){
                $html .="".select_form($kol,$data,$data["isi"]);                
            }else if($data["jenis"]=="file"){
                $html .="".upload_form($kol,$data);
            }else if($data["jenis"]=="textarea"){
                $html .="".textarea_form($kol,$data);
            }
        }
        $html .="".submit_form();
    $html .="".footer_form();
    $html .='</form>';


    return $html;

}

function upload_form($name,$jenis){
    $html='
    <div class="form-example-int">
        <div class="form-group">
            <label>'.$jenis["label"].'</label>
            <div class="nk-int-st">
            <input  required="" type="file" name="uploads" />
            </div>
        </div>
    </div>';
    return $html;
}



function input_form($name,$jenis){
    $html='
    <div class="form-example-int">
        <div class="form-group">
            <label>'.$jenis["label"].'</label>
            <div class="nk-int-st">
                <input  required="" type="text" name="inputan['.$name.']" class="form-control input-sm" placeholder="'.$jenis["placeholder"].'">
            </div>
        </div>
    </div>';
    return $html;
}


function textarea_form($name,$jenis){
    $html='
    <div class="form-example-int">
        <div class="form-group">
            <label>'.$jenis["label"].'</label>
            <div class="nk-int-st">
                <textarea  required=""  name="inputan['.$name.']" class="form-control input-sm" placeholder="'.$jenis["placeholder"].'"></textarea>
            </div>
        </div>
    </div>';
    return $html;
}

function select_form($name,$jenis,$array){
    $option="";
    foreach($array as $a){
             $option .='<option value='.$a->$jenis["id"].'>'.$a->$jenis["value"].'</option>';
        
    }

    $html='
    <div class="form-example-int">
        <div class="form-group">
            <label>'.$jenis["label"].'</label>
            <div class="nk-int-st">
                <select required="" class="form-control" name="inputan['.$name.']">
                <option value="">Silahkan Pilih</option>
                '.$option.'
                </select>
            </div>
        </div>
    </div>';
    return $html;
}




function submit_form(){
    $html='<div align="right" class="form-example-int mg-t-15">
                <button class="btn btn-success notika-btn-success">Simpan Data</button>
            </div>';
    return $html;
}



function header_form(){
   
    $html='
    <div class="form-example-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-example-wrap mg-t-30">
    ';
    return $html;
}
function footer_form(){
    $html='
                    </div>
                </div>
            </div>
        </div>
    </div>
    ';
    return $html;
}



?>