<?php

class Model_design extends CI_Model {

    function __construct()
	{
        parent::__construct();
        $this->load->model("model_crud");
    }
    

    function buat_table_dan_isinya($table,$id,$kolom,$action,$base,$to,$buttoncustom=""){
        $data="";
        $i=1;
        $data=$this->model_crud->getdata($table);
        $tampilan =template_data_table($table,$id,$data,$kolom,$action,$base,$to,$buttoncustom);
        
        return $tampilan;
    }
    function buat_form_tambah($table,$kolom,$base){

        $tampilan =template_tambah_form($table,$kolom,$base);
        return $tampilan;
    }

    function buat_form_edit($table,$kolom,$base,$id,$kolom_id){
        $kondisi["".$kolom_id]=$id;
        $data=$this->model_crud->getdata($table,'',$kondisi);
        $tampilan =template_edit_form($table,$kolom,$base,$data,$kolom_id);
        return $tampilan;
    }









    

}