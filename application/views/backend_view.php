<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="<?=backend_asset_css("bootstrap.min.css")?>">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="<?=backend_asset_css("font-awesome.min.css")?>">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="<?=backend_asset_css("owl.carousel.css")?>">
    <link rel="stylesheet" href="<?=backend_asset_css("owl.theme.css")?>">
    <link rel="stylesheet" href="<?=backend_asset_css("owl.transitions.css")?>">
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="<?=backend_asset_css("meanmenu/meanmenu.min.css")?>">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="<?=backend_asset_css("animate.css")?>">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="<?=backend_asset_css("normalize.css")?>">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="<?=backend_asset_css("scrollbar/jquery.mCustomScrollbar.min.css")?>">
    <!-- jvectormap CSS
		============================================ -->
    <link rel="stylesheet" href="<?=backend_asset_css("jvectormap/jquery-jvectormap-2.0.3.css")?>">
    <!-- notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="<?=backend_asset_css("notika-custom-icon.css")?>">
    <!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="<?=backend_asset_css("wave/waves.min.css")?>">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="<?=backend_asset_css("main.css")?>">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="<?=backend_asset_css("style.css")?>">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="<?=backend_asset_css("responsive.css")?>">
    <!-- modernizr JS
		============================================ -->
    <script src="<?=backend_asset_js("vendor/modernizr-2.8.3.min.js")?>"></script>
    <link rel="stylesheet"  href="<?=backend_asset_vendor("jquery_confrim/jquery-confirm.min.css")?>">
    <link rel="stylesheet" href="<?=backend_asset_css("jquery.dataTables.min.css")?>">

    <link rel="stylesheet" href="<?=backend_asset_vendor("parsley/parsley.css")?>">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">



    
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Start Header Top Area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <a href="#"><img src="img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav">
                            
                            <li class="nav-item text-sm"><a href="<?=base_url()?>login/logout" ><span>Logout</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    <!-- Mobile Menu start -->
    <?php print menu_mobile()?>
    <!-- Mobile Menu end -->
    <!-- Main Menu area start-->
    <?php print menu_default()?>
      
	<?php print isset($content) ? $content : ''; ?>



    <!-- End Realtime sts area-->
    <!-- Start Footer area-->
<div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-copy-right">
                        <p>Copyright © 2018 
. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer area-->
    <!-- jquery
		============================================ -->
    <script src="<?=backend_asset_js("vendor/jquery-1.12.4.min.js")?>"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="<?=backend_asset_js("bootstrap.min.js")?>"></script>
    <!-- wow JS
		============================================ -->
    <script src="<?=backend_asset_js("wow.min.js")?>"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="<?=backend_asset_js("jquery-price-slider.js")?>"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="<?=backend_asset_js("owl.carousel.min.js")?>"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="<?=backend_asset_js("jquery.scrollUp.min.js")?>"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="<?=backend_asset_js("meanmenu/jquery.meanmenu.js")?>"></script>
    <!-- counterup JS
		============================================ -->
    <script src="<?=backend_asset_js("counterup/jquery.counterup.min.js")?>"></script>
    <script src="<?=backend_asset_js("counterup/waypoints.min.js")?>"></script>
    <script src="<?=backend_asset_js("counterup/counterup-active.js")?>"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="<?=backend_asset_js("scrollbar/jquery.mCustomScrollbar.concat.min.js")?>"></script>
    <!-- jvectormap JS
		============================================ -->
    <script src="<?=backend_asset_js("jvectormap/jquery-jvectormap-2.0.2.min.js")?>"></script>
    <script src="<?=backend_asset_js("jvectormap/jquery-jvectormap-world-mill-en.js")?>"></script>
    <script src="<?=backend_asset_js("jvectormap/jvectormap-active.js")?>"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="<?=backend_asset_js("sparkline/jquery.sparkline.min.js")?>"></script>
    <script src="<?=backend_asset_js("sparkline/sparkline-active.js")?>"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="<?=backend_asset_js("flot/jquery.flot.js")?>"></script>
    <script src="<?=backend_asset_js("flot/jquery.flot.resize.js")?>"></script>
    <script src="<?=backend_asset_js("flot/curvedLines.js")?>"></script>
    <script src="<?=backend_asset_js("flot/flot-active.js")?>"></script>
    <!-- knob JS
		============================================ -->
    <script src="<?=backend_asset_js("knob/jquery.knob.js")?>"></script>
    <script src="<?=backend_asset_js("knob/jquery.appear.js")?>"></script>
    <script src="<?=backend_asset_js("knob/knob-active.js")?>"></script>
    <!--  wave JS
		============================================ -->
    <script src="<?=backend_asset_js("wave/waves.min.js")?>"></script>
    <script src="<?=backend_asset_js("wave/wave-active.js")?>"></script>
    <!--  todo JS
		============================================ -->
    <script src="<?=backend_asset_js("todo/jquery.todo.js")?>"></script>
    <!-- plugins JS
		============================================ -->
    <script src="<?=backend_asset_js("plugins.js")?>"></script>
	<!--  Chat JS
		============================================ -->
    <!-- main JS
		============================================ -->
    <script src="<?=backend_asset_js("main.js")?>"></script>
	<!-- tawk chat JS
		============================================ -->
    <script src="<?= backend_asset_js("data-table/jquery.dataTables.min.js")?>"></script>


    <script src="<?= backend_asset_vendor("jquery_confrim/jquery-confirm.min.js")?>"></script>
    <script src="<?= backend_asset_vendor("parsley/parsley.js")?>"></script>

</body>


<script>

$(".btn_hapus").click(function() {
           var id= $(this).attr("data-id");
           var table= $(this).attr("data-table");
           var kolom_id= $(this).attr("data-kolom_id");
        
        $.confirm({
            title: 'Hapus !',
            content: 'Anda yakin menghapus data ini!',
            buttons: {
                confirm: function () {                    
                    $.ajax({
                        type : "POST",
                        url  : "<?php echo base("crud_controllers/hapus")?>",
                        dataType : "JSON",
                        data : {id:id,table:table,kolom_id,kolom_id},
                        success: function(data){
                              window.location.href =data; 
                        }
                    });
                },
                cancel: function () {
                    $.alert('Dibatalkan!');
                }
                
            }
        });


});


$(document).ready(function() {
		 $('#data-table').DataTable();
});
$('#form').parsley();


</script>

</html>