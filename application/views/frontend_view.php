<style>
.menu{
    vertical-align: middle;
    line-height: 40px;
}
</style>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Frontend</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?=frontend_assets("css/styles.min.css")?>">
    <link rel="stylesheet"  href="<?=backend_asset_vendor("slider/css/lightslider.css")?>"/>

</head>

<body style="font-family:roboto">
    <header>
        <div class="row">
            <div class="col">
                <nav class="navbar navbar-dark navbar-expand-md fixed-top bg-dark">
                    <div class="container-fluid"><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse" id="navcol-1">
                            <ul class="nav navbar-nav flex-grow-1 justify-content-between">
                                <li class="nav-item" role="presentation"><a class="nav-link" href="<?=base_url()?>frontend/beranda"><img  width="50px" class="img-fluid" src="<?=base_url()?>/assets/tampilan/frontend/img/logo.png"> </a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link menu" href="<?=base_url()?>frontend/produk">Mitsubishi</a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link menu" href="#">Tentang Kami</a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link menu" href="#">Price List</a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link menu" href="#">Promo Kredit</a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link menu" href="#">Artikel</a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link menu" href="#">Facebook</a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link menu" href="#">Hubungi Kami</a></li>
                               
                            </ul>
                        </div>
                    </div>
                </nav>
                <nav class="navbar navbar-dark navbar-expand-md fixed-top bg-dark">
                    <div class="container-fluid"><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse" id="navcol-1">
                            <ul class="nav navbar-nav flex-grow-1 justify-content-between">
                                <li class="nav-item" role="presentation"><a class="nav-link" href="<?=base_url()?>frontend/beranda"><img width="50px" class="img-fluid" src="<?=base_url()?>/assets/tampilan/frontend/img/logo.png"></a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link menu" href="<?=base_url()?>frontend/produk">Mitsubishi</a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link menu" href="#">Tentang Kami</a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link menu" href="#">Price List</a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link menu" href="#">Promo Kredit</a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link menu" href="#">Artikel</a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link menu" href="#">Facebook</a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link menu" href="#">Hubungi Kami</a></li>
                              
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </header>
 

	<?php print isset($content) ? $content : ''; ?>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <script src="<?=frontend_assets("js/script.min.js")?>"></script>   
    <script src="<?=backend_asset_vendor("slider/js/lightslider.js")?>"></script> 

          <script>
    	
    </script>
</body>

</html>