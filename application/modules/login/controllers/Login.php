<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
   
    private $app_name = "login";
    private $template_page = "login_view";

    public function __construct() {
		parent::__construct($this->app_name);
        $this->load->library('template');
        $this->template->set('controller', $this);
     //   $this->load->library('Auth_AD');
        $this->load->model('Model_login');


	}
	public function index() {
    $data="";
    $this->template->load($this->template_page, 'login', $data);
    if(isset($_SESSION['SESS_userID'])){
      redirect(base_url()."beranda");
    }
  }


  public function logout() {
    if(isset($_SESSION['SESS_userID'])){
      $this->session->unset_userdata('username');
      $this->session->unset_userdata('password');
      $this->session->sess_destroy();
      redirect(base_url()."login");
    }
  }

  public function login_proses2(){
    $kondisi["username"] = $_POST["username"];
    $kondisi["password"] = md5($_POST["pass"]);
    $login=$this->Model_login->getdata('users','',$kondisi);    
    if(count($login)!=0){
      foreach($login as $DATA_user)
				{
					$_SESSION['SESS_userID'] = $DATA_user->id;
					$_SESSION['SESS_userName'] = $DATA_user->username;				
					$_SESSION['SESS_userPass'] = $DATA_user->password;				
        }
        echo 1;
      
    }




        
 }
  

}
