<style>

.carousel-fade .carousel-item {
 opacity: 0;
 transition-duration: .6s;
 transition-property: opacity;
}

.carousel-fade  .carousel-item.active,
.carousel-fade  .carousel-item-next.carousel-item-left,
.carousel-fade  .carousel-item-prev.carousel-item-right {
  opacity: 1;
}

.carousel-fade .active.carousel-item-left,
.carousel-fade  .active.carousel-item-right {
 opacity: 0;
}

.carousel-fade  .carousel-item-next,
.carousel-fade .carousel-item-prev,
.carousel-fade .carousel-item.active,
.carousel-fade .active.carousel-item-left,
.carousel-fade  .active.carousel-item-prev {
 transform: translateX(0);
 transform: translate3d(0, 0, 0);
}

</style>



   <div  class="carousel  slide" style="padding-top:30px" data-ride="carousel" id="carousel-1">
   	<div class="carousel-inner carousel-fade" role="listbox">
   		<?php
        $x=1;
        foreach($banner as $data){
            if($x==1){
         ?>
   		<div class="carousel-item active"><img class="w-100 d-block"  src="<?=base_url()?>uploads/<?=$data->image?>"
   				alt="Slide Image"></div>
   		<?php
            }else{
        ?>
   		<div class="carousel-item "><img class="w-100 d-block" src="<?=base_url()?>uploads/<?=$data->image?>"
   				alt="Slide Image"></div>
   		<?php
            
            }
        $x++;        
        }
        ?>

   	</div>
   	<div><a class="carousel-control-prev" href="#carousel-1" role="button" data-slide="prev"><span
   				class="carousel-control-prev-icon"></span><span class="sr-only">Previous</span></a><a
   			class="carousel-control-next" href="#carousel-1" role="button" data-slide="next"><span
   				class="carousel-control-next-icon"></span><span class="sr-only">Next</span></a></div>

   	<ol class="carousel-indicators">
   		<?php 
        for($x=0;$x<count($banner);$x++){
            if($x==0){
        ?>
   		<li data-target="#carousel-1 active" data-slide-to="<?=$x?>"></li>
   		<?php
            }else{
                ?>
   		<li data-target="#carousel-1" data-slide-to="<?=$x?>"></li>

   		<?php
            }
        }
        ?>
   	</ol>
   </div>
   <div class="features-clean">
   	<div class="container">
   		<div class="intro">
   		
   		</div>
   		<div class="row features">
   			<div class="col-sm-6 col-lg-3 item"><i class="fa fa-search icon"></i>
   				<h4 class="name">Cari Mobil</h4>
   				<p class="description">Anda sedang mencari mobil impian anda</p>
   			</div>
   			<div class="col-sm-6 col-lg-3 item"><i class="fa fa-phone icon"></i>
   				<h3 class="name">Hubungi Kami</h3>
   				<p class="description">Segera hubungi kami jika anda sudah menemukan mobil impian anda !</p>
   			</div>
   			<div class="col-sm-6 col-lg-3 item"><i class="fa fa-list-alt icon"></i>
   				<h4 class="name">Transaksi Mudah</h4>
   				<p class="description">Sangat mudah dengan bertransaksi denga kami.</p>
   			</div>
               <div class="col-sm-6 col-lg-3 item"><i class="fa fa-key icon"></i>
   				<h4 class="name">Serah Terima Mobil</h4>
   				<p class="description">Dengan banyak kemudahan anda akan langsung memiliki mobil impian anda .</p>
   			</div>
   			
   		</div>
   	</div>
   </div>
   <div class="container">
   	<div class="row product-list dev">


   		<?php 
            foreach($produk as $p){
         ?>

   		<div class="col-sm-6 col-md-4 product-item animation-element slide-top-left">
   			<div class="product-container">
   				<div class="row">
   					<div class="col-md-12"><a class="product-image" href="#"><img class="img-fluid"
   								src="<?=uploadan($p->image)?>"></a></div>
   				</div>
   				<div class="row">
   					<div class="col-8">
   						<h2><a href="#"><?=$p->nama_produk?></a></h2>
   					</div>
   				</div>
   				<div class="product-rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i
   						class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half"></i><a
   						class="small-text"><?=$p->banyak_type?> type avaliable</a></div>
   				<div class="row">
   					<div class="col-12">
   						<p class="product-description"><?=$p->deskripsi?> </p>
   						<div class="row">
   							<div class="col-4">
   								<button class="btn btn-danger" type="button">Lihat Detail!</button>
   							</div>
   							<div class="col-8">
   								<p class="product-price">Rp. <?=$p->harga?> </p>
   							</div>
   						</div>
   					</div>
   				</div>
   			</div>
   		</div>
   		<?php
            }
            ?>

   	</div>
   </div>
  
   <div class="article-list">
   	<div class="container">
   		<div class="intro">
   			<h2 class="text-center">Artikel Otomotif</h2>
   			<p class="text-center">Artikel - Artikel di dunia otomaotif  </p>
   		</div>
   		<div class="row articles">
           <?php
           foreach($article as $a){
           ?>
   			<div class="col-sm-6 col-md-4 item"><a href="#"><img class="img-fluid" src="<?=uploadan($a->image)?>"></a>
   				<h3 class="name"><?=$a->judul_article?></h3>
   				<p class="description"><?=$a->isi_article?></p>
                 
   			</div>
            <?php
           }
            ?>
   		
   		</div>
   	</div>
   </div>

   <div class="testimonials-clean">
   	<div class="container">
   		<div class="intro">
   			<h2 class="text-center">Testimoni Pelanggan </h2>
   			<p class="text-center">Testimony pelanggan pelanggan kami</p>
   		</div>
   		<div class="row people">
   			
               
           <?php 
                foreach($testimony as $testi){
                ?>   
            <div class="col-md-6 col-lg-4 item">
   				<div class="box">
   					<p class="description"><?=$testi->pesan?></p>
   				</div>
   				<div class="author"><img class="rounded-circle" src="<?=uploadan($testi->image)?>">
   					<h5 class="name"><?=$testi->nama_user?></h5>
   					<p class="title"><?=$testi->mobil?></p>
   				</div>
   			</div>
            <?php
                    }
            ?>
   			
   		</div>
   	</div>
   </div>
  
   <div>
   	<div class="container">
   		<div class="row">
   			<div class="col-8 col-md-8">
   				<div class="intro">
   					<h2>Hubungi Kami !</h2>
   					<p class="text-dark"><br>Dealer kami menjual mobil Mitsubishi Xpander, Pajero Sport,
   						Delica,&nbsp;Outlander Sport&nbsp;dan&nbsp;Mirage, dan Triton, dan masih banyak lagi.
   						Opportunity penjualan kendaraan-kendaraan penumpang disetiap tahunnya sangat
   						besar, oleh karena itu lokasi dealer yang strategis, produk-produk berkualitas dan pelayanan
   						prima akan sangat mendukung suksesnya penjualan Mitsubishi.Selain itu dengan kehadiran dealer
   						3S ini, kami berharap dapat memenuhi
   						kebutuhan kendaraan-kendaraan di area-area strategis, seperti area padat penduduk, distrik
   						bisnis hingga shopping mall dengan layanan penjualan sesuai standar Mitsubishi.<br><br></p>
   				</div>
   			</div>
   			<div class="col">
   				<div class="card text-center">
   					<div class="card-body"><img class="img-fluid" src="<?=frontend_assets("img/im.jpg")?>">
   						<h5 class="text-center card-title">Inneke Kosmawati</h5>
   						<h6 class="text-center text-muted card-subtitle mb-2">Sales Excekutive</h6><button
   							class="btn btn-primary btn-block" type="button"><i class="fa fa-phone"></i>&nbsp;Hubungi
   							Kami<br></button>
   					</div>
   				</div>
   			</div>
   		</div>
   	</div>
   </div>
