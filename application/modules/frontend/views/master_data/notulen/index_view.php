<?=template_breadcump($title,$deskripsi,'')?>

<?=header_form()?>

<div class="data-table-area">
    <div class="container">
        <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                <div class="breadcomb-report">
            <a href=''  data-toggle="tooltip" data-placement="left" title="Tambah Data" class="btn">Tambah Data</a>
            </div>
                <div class="table-responsive">
                        <table id="data-table" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No Weekly</th>
                                    <th>No RKM </th>
                                    <th>Dokumen</th>
                                    <th>Log By</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach($rkm_no_tulen as $rkm){
                                ?>
                                <tr>
                                    <td><?=$rkm->no_rkm?></td>
                                    <td><?=$rkm->no_rkmnotulen?></td>
                                    <td><?=$rkm->nama_file?></td>
                                    <td><?=$rkm->log_by?></td>
                                    <td><button data-kolom_id="id" data-id='<?=$rkm->id?>' data-table='rkm_notulen'  data-toggle="tooltip" data-placement="left" title="Hapus Data" class="btn btn-sm btn-danger btn_hapus ">Hapus</button></td>
                                <?php    
                                }
                                ?>

                            </tbody>
                           
                        </table>
                    </div>

                </div>
        </div>
        </div>
</div>


<?=footer_form()?>