<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {
   
    private $app_name = "beranda";
    private $template_page = "frontend_view";

    public function __construct() {
		parent::__construct($this->app_name);
        $this->load->library('template');
        $this->load->model('model_crud');
        $this->template->set('controller', $this);
	}
	public function index() {
    $data="";
    $data["banner"]=$this->model_crud->getdata("banner");
    $data["produk"]=$this->model_crud->getdata("produk");
    $data["testimony"]=$this->model_crud->getdata("testimony");
    $data["article"]=$this->model_crud->getdata("article");
    $this->template->load($this->template_page, 'tampilan/index_view', $data);
  }
}