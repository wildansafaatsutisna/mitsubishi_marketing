<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {
   
    private $app_name = "beranda";
    private $template_page = "frontend_view";

    public function __construct() {
		parent::__construct($this->app_name);
        $this->load->library('template');
        $this->load->model('model_crud');
        $this->template->set('controller', $this);
	}
	public function index() {
    $data="";
    $data["banner"]=$this->model_crud->getdata("banner");
    $data["produk"]=$this->model_crud->getdata("produk");
    $this->template->load($this->template_page, 'tampilan/produk_view', $data);
  }
  public function detail_produk($id) {
    $kondisi["id_produk"]=$id;
    $data["produk"]=$this->model_crud->getdata("produk","",$kondisi)[0];
    $this->template->load($this->template_page, 'tampilan/detail_produk', $data);
  }
}