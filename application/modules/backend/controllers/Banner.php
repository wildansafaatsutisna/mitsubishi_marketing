<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {
   
    private $tabel = "banner";
    private $template_page = "backend_view";
    private $base = "";
    private $action=['tambah','edit','hapus'];

  
   //array form
   
   private $form = array(
    "image" => array(
      "jenis"=>"file",
      "label"=>"Gambar",
      "view"=>"uploads",

     ),
    "nama_banner" => array(
      "jenis"=>"input",
      "label"=>"Nama Banner",
      "view"=>"nama_banner",
      "placeholder"=>"Masukan Nama Banner"
     ),
    "deskripsi" => array(
      "jenis"=>"textarea",
      "view"=>"deskripsi",
      "label"=>"Deskripsi",
      "placeholder"=>"Masukan Deskripsi"
     )
  );
  
  
  
    //isian ke datatable
    private $kolom = array(
      "image" => array(
        "jenis"=>"image",
        "label"=>"Gambar",
        "view"=>"image",

      ),
      "nama_banner" => array(
        "jenis"=>"text",
        "label"=>"Nama Banner",
        "view"=>"nama_banner"

       ),
      "deskripsi" => array(
        "jenis"=>"text",
        "label"=>"Deskripsi",
        "view"=>"deskripsi"
       )
  
    );



    public function __construct() {
		parent::__construct();
        $this->load->library('template');
        $this->template->set('controller', $this);
        $this->load->model('model_design');
        if(!isset($_SESSION['SESS_userID'])){
          redirect(base_url()."login");
        }
        $this->base="".base_url()."backend/";

  }
	public function index() {
    //breadcump
    $data["title"]="Data Master Banner";
    $data["deskripsi"]="Pengelolaan data master Banner";
    $data["table_tampilan"]=$this->model_design->buat_table_dan_isinya($this->tabel,'id_banner',$this->kolom,$this->action,$this->base,$this->tabel);
    $this->template->load($this->template_page, 'master_data/master/index_view', $data);
  }
  
  public function tambah_banner(){
    $this->base="".base_url()."backend/banner";

    $data["title"]="Tambah Data Master Banner";
    $data["deskripsi"]="Silahkan isi form dibawah ini";
    $data["form_tambah"]=$this->model_design->buat_form_tambah($this->tabel,$this->form,$this->base);
    $this->template->load($this->template_page, 'master_data/master/tambah_view', $data);
  }


  public function edit_banner($id){
    $data["title"]="Edit Data Master banner";
    $data["deskripsi"]="Silahkan isi form dibawah ini";
    $this->base = "".base_url()."backend/banner/";
    $data["form_edit"]=$this->model_design->buat_form_edit($this->tabel,$this->form,$this->base,$id,'id_banner');
    $this->template->load($this->template_page, 'master_data/master/edit_view', $data);
  }

  
}