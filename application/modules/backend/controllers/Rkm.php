<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rkm extends CI_Controller {
   
    private $app_name = "rkm";
    private $template_page = "frontend_view";

    public function __construct() {
		parent::__construct($this->app_name);
        $this->load->library('template');
        $this->template->set('controller', $this);
        $this->load->model('model_design');
        if(!isset($_SESSION['SESS_userID'])){
          redirect(base_url()."login");
        }

  }
	public function index() {
    //breadcump
    $data["title"]="Data Master RKM";
    $data["deskripsi"]="Pengelolaan data master RKM";
    //base
    $base = "".base_url()."frontend/";
    //aksi
    $action=['tambah','edit','hapus'];
    //arahkan ke
    $to="rkm";
    //isi table
    $kolom_header=['No RKM','Periode','Jam','Tempat'];
    $kolom=['no_rkm','periode','jam','tempat'];
    $data["table_tampilan"]=$this->model_design->buat_table_dan_isinya("rkm",'id',$kolom_header,$kolom,$action,$base,$to);
    $this->template->load($this->template_page, 'master_data/rkm/index_view', $data);
  }
  
  public function tambah_rkm(){
    $data["title"]="Tambah Data Master RKM";
    $data["deskripsi"]="Silahkan isi form dibawah ini";
    $base = "".base_url()."frontend/rkm/";
    $inputan = array(
      "no_rkm" => array(
        "jenis"=>"input",
        "label"=>"No RKM",
        "placeholder"=>"Masukan No RKM"
       ),
      "periode" => array(
        "jenis"=>"input",
        "label"=>"Periode",
        "placeholder"=>"Masukan Periode"
       ),
      "jam" => array(
        "jenis"=>"input",
        "label"=>"Jam",
        "placeholder"=>"Masukan Jam"

       ),
      "tempat" => array(
        "jenis"=>"input",
        "label"=>"Tempat",
        "placeholder"=>"Masukan Tempat"
       ),
    );
    $data["form_tambah"]=$this->model_design->buat_form_tambah('rkm',$inputan,$base);
    $this->template->load($this->template_page, 'master_data/rkm/tambah_view', $data);
  }


  public function edit_rkm($id){
    $data["title"]="Edit Data Master RKM";
    $data["deskripsi"]="Silahkan isi form dibawah ini";
    $base = "".base_url()."frontend/rkm/";
    $inputan = array(
      "periode" => array(
        "jenis"=>"input",
        "label"=>"Periode",
        "placeholder"=>"Masukan Periode"
       ),
      "jam" => array(
        "jenis"=>"input",
        "label"=>"Jam",
        "placeholder"=>"Masukan Jam"
       ),
      "tempat" => array(
        "jenis"=>"input",
        "label"=>"Tempat",
        "placeholder"=>"Masukan Tempat"
       ),
    );
    $data["form_edit"]=$this->model_design->buat_form_edit('rkm',$inputan,$base,$id,'id');
    $this->template->load($this->template_page, 'master_data/rkm/edit_view', $data);
  }

  
}