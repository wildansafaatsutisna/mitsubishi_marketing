<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {
   
    private $tabel = "produk";
    private $template_page = "backend_view";
    private $base = "";
    private $action=['tambah','edit','hapus'];
    private $buttoncustom="";

  
   //array form
   
   private $form ="";
  
  
  
    //isian ke datatable
    private $kolom = array(
      "image" => array(
        "jenis"=>"image",
        "label"=>"Gambar",
        "view"=>"image",

      ),
      "nama_produk" => array(
        "jenis"=>"text",
        "label"=>"Nama Produk",
        "view"=>"nama_produk"

       ),
       "banyak_type" => array(
        "jenis"=>"text",
        "label"=>"Banyak Type",
        "view"=>"banyak_type"
       ),
      "deskripsi" => array(
        "jenis"=>"text",
        "label"=>"Deskripsi",
        "view"=>"deskripsi"
      ),
      "harga" => array(
        "jenis"=>"text",
        "label"=>"Harga",
        "view"=>"harga"
      ),
      
  
    );



    public function __construct() {
		parent::__construct();
        $this->load->library('template');
        $this->template->set('controller', $this);
        $this->load->model('model_design');
        if(!isset($_SESSION['SESS_userID'])){
          redirect(base_url()."login");
        }
        $this->base="".base_url()."backend/";

        $this->buttoncustom=array("btn_exterior"=>array(
                                  "button","href",
                                  "".base_url()."frontend/produk/detail_exterior",
                                  "id_produk",
                                  "Exterior",
                                  "btn-primary"
                                
                                ),
                                  "btn_interior"=>array(
                                    "button","href",
                                    "".base_url()."frontend/produk/detail_interior",
                                    "id_produk",
                                    "Interior",
                                    "btn-success"

                                  ),
                                );
        
        $this->form= array(
          "image" => array(
            "jenis"=>"file",
            "label"=>"Gambar",
            "view"=>"uploads",
           ),
          "nama_produk" => array(
            "jenis"=>"input",
            "label"=>"Nama Produk",
            "view"=>"nama_produk",
            "placeholder"=>"Masukan Nama Produk"
           ),
          "deskripsi" => array(
            "jenis"=>"textarea",
            "view"=>"deskripsi",
            "label"=>"Deskripsi",
            "placeholder"=>"Masukan Deskripsi"
          ),
          "id_kategori" => array(
            "jenis"=>"select",
            "view"=>"id_kategori",
            "label"=>"Pilih Kategori",
            "placeholder"=>"Masukan Deskripsi",
            "isi" => $this->model_crud->getdata("kategori"), 
            "value" => "nama_kategori", //value 
            "id" => "id_kategori", //id
          ),
          "banyak_type" => array(
            "jenis"=>"input",
            "label"=>"Banyak Type",
            "view"=>"banyak_type",
            "placeholder"=>"Jumlah Type Produk"
           ),
           "harga" => array(
             "jenis"=>"input",
             "label"=>"Harga",
             "view"=>"harga",
             "placeholder"=>"Masukan Harga"
            )
        );

  }
	public function index() {
    //breadcump
    $data["title"]="Data Master Produk";
    $data["deskripsi"]="Pengelolaan data Master Produk";
    $data["table_tampilan"]=$this->model_design->buat_table_dan_isinya($this->tabel,'id_produk',$this->kolom,$this->action,$this->base,$this->tabel,$this->buttoncustom);
    $this->template->load($this->template_page, 'master_data/master/index_view', $data);
  }
  
  public function tambah_produk(){
    $this->base="".base_url()."backend/produk";

    $data["title"]="Tambah Data Master Produk";
    $data["deskripsi"]="Silahkan isi form dibawah ini";
    $data["form_tambah"]=$this->model_design->buat_form_tambah($this->tabel,$this->form,$this->base);
    $this->template->load($this->template_page, 'master_data/master/tambah_view', $data);
  }


  public function edit_produk($id){
    $data["title"]="Edit Data Master produk";
    $data["deskripsi"]="Silahkan isi form dibawah ini";
    $this->base = "".base_url()."backend/produk/";
    $data["form_edit"]=$this->model_design->buat_form_edit($this->tabel,$this->form,$this->base,$id,'id_produk');
    $this->template->load($this->template_page, 'master_data/master/edit_view', $data);
  }

  
}