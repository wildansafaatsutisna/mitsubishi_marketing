<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {
   
    private $tabel = "kategori";
    private $template_page = "backend_view";
    private $base = "";
    private $action=['tambah','edit','hapus'];

  
   //array form
   
   private $form = array(
   
    "nama_kategori" => array(
      "jenis"=>"input",
      "label"=>"Nama Kategori",
      "view"=>"nama_kategori",
      "placeholder"=>"Masukan Nama Kategori"
     )
  );
  
  
  
    //isian ke datatable
    private $kolom = array(
   
      "nama_kategori" => array(
        "jenis"=>"text",
        "label"=>"Nama Kategori",
        "view"=>"nama_kategori"

       )
  
    );



    public function __construct() {
		parent::__construct();
        $this->load->library('template');
        $this->template->set('controller', $this);
        $this->load->model('model_design');
        if(!isset($_SESSION['SESS_userID'])){
          redirect(base_url()."login");
        }
        $this->base="".base_url()."backend/";

  }
	public function index() {
    //breadcump
    $data["title"]="Data Master Kategori";
    $data["deskripsi"]="Pengelolaan data Master Kategori";
    $data["table_tampilan"]=$this->model_design->buat_table_dan_isinya($this->tabel,'id_kategori',$this->kolom,$this->action,$this->base,$this->tabel);
    $this->template->load($this->template_page, 'master_data/master/index_view', $data);
  }
  
  public function tambah_kategori(){
    $this->base="".base_url()."backend/kategori";

    $data["title"]="Tambah Data Master Kategori";
    $data["deskripsi"]="Silahkan isi form dibawah ini";
    $data["form_tambah"]=$this->model_design->buat_form_tambah($this->tabel,$this->form,$this->base);
    $this->template->load($this->template_page, 'master_data/master/tambah_view', $data);
  }


  public function edit_kategori($id){
    $data["title"]="Edit Data Master Kategori";
    $data["deskripsi"]="Silahkan isi form dibawah ini";
    $this->base = "".base_url()."backend/kategori/";
    $data["form_edit"]=$this->model_design->buat_form_edit($this->tabel,$this->form,$this->base,$id,'id_kategori');
    $this->template->load($this->template_page, 'master_data/master/edit_view', $data);
  }

  
}