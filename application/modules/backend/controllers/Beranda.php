<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {
   
    private $app_name = "beranda";
    private $template_page = "backend_view";

    public function __construct() {
		parent::__construct($this->app_name);
        $this->load->library('template');
        $this->template->set('controller', $this);
	}
	public function index() {
    $data="";
    $this->template->load($this->template_page, 'beranda/index_view', $data);
  }
}