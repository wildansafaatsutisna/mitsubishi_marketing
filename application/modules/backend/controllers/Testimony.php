<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimony extends CI_Controller {
   
    private $tabel = "testimony";
    private $template_page = "backend_view";
    private $base = "";
    private $action=['tambah','edit','hapus'];

  
   //array form
   
   private $form ="";
  
  
  
    //isian ke datatable
    private $kolom = array(
      "image" => array(
        "jenis"=>"image",
        "label"=>"Foto Profile",
        "view"=>"image",

      ),
      "nama_user" => array(
        "jenis"=>"text",
        "label"=>"Nama User",
        "view"=>"nama_user"

      ),
       "mobil" => array(
        "jenis"=>"text",
        "label"=>"Mobil",
        "view"=>"mobil"
       ),
       "pesan" => array(
        "jenis"=>"text",
        "label"=>"Pesan",
        "view"=>"pesan"
       )
      
  
    );



    public function __construct() {
		parent::__construct();
        $this->load->library('template');
        $this->template->set('controller', $this);
        $this->load->model('model_design');
        if(!isset($_SESSION['SESS_userID'])){
          redirect(base_url()."login");
        }
        $this->base="".base_url()."backend/";

        $this->form= array(
          "image" => array(
            "jenis"=>"file",
            "label"=>"Foto Profile",
            "view"=>"image",
           ),
          "nama_user" => array(
            "jenis"=>"input",
            "label"=>"Nama User",
            "view"=>"nama_user",
            "placeholder"=>"Masukan Nama User"
           ),
           "mobil" => array(
             "jenis"=>"input",
             "view"=>"mobil",
             "label"=>"Mobil",
             "placeholder"=>"Masukan Jenis Mobil "
           ),
            "pesan" => array(
            "jenis"=>"textarea",
            "view"=>"pesan",
            "label"=>"Pesan",
            "placeholder"=>"Masukan Pesan "
          )
        );

  }
	public function index() {
    //breadcump
    $data["title"]="Data Master Testimony";
    $data["deskripsi"]="Pengelolaan Data Master Testimony";
    $data["table_tampilan"]=$this->model_design->buat_table_dan_isinya($this->tabel,'id_testimony',$this->kolom,$this->action,$this->base,$this->tabel);
    $this->template->load($this->template_page, 'master_data/master/index_view', $data);
  }
  
  public function tambah_testimony(){
    $this->base="".base_url()."backend/testimony";

    $data["title"]="Tambah Data Master Testimony";
    $data["deskripsi"]="Silahkan isi form dibawah ini";
    $data["form_tambah"]=$this->model_design->buat_form_tambah($this->tabel,$this->form,$this->base);
    $this->template->load($this->template_page, 'master_data/master/tambah_view', $data);
  }


  public function edit_testimony($id){
    $data["title"]="Edit Data Master Testimony";
    $data["deskripsi"]="Silahkan isi form dibawah ini";
    $this->base = "".base_url()."backend/testimony/";
    $data["form_edit"]=$this->model_design->buat_form_edit($this->tabel,$this->form,$this->base,$id,'id_testimony');
    $this->template->load($this->template_page, 'master_data/master/edit_view', $data);
  }

  
}