<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {
   
    private $tabel = "article";
    private $template_page = "backend_view";
    private $base = "";
    private $action=['tambah','edit','hapus'];

  
   //array form
   
   private $form ="";
  
  
  
    //isian ke datatable
    private $kolom = array(
      "image" => array(
        "jenis"=>"image",
        "label"=>"Gambar",
        "view"=>"image",

      ),
      "judul_article" => array(
        "jenis"=>"text",
        "label"=>"Judul Article",
        "view"=>"judul_article"

      ),
       "isi_article" => array(
        "jenis"=>"text",
        "label"=>"Deskripsi",
        "view"=>"isi_article"
       )
    );



    public function __construct() {
		parent::__construct();
        $this->load->library('template');
        $this->template->set('controller', $this);
        $this->load->model('model_design');
        if(!isset($_SESSION['SESS_userID'])){
          redirect(base_url()."login");
        }
        $this->base="".base_url()."backend/";

        $this->form= array(
          "image" => array(
            "jenis"=>"file",
            "label"=>"Gambar Article",
            "view"=>"image",
           ),
          "judul_article" => array(
            "jenis"=>"input",
            "label"=>"Judul Article",
            "view"=>"judul_article",
            "placeholder"=>"Masukan Judul Article"
           ),
            "isi_article" => array(
            "jenis"=>"textarea",
            "view"=>"isi_article",
            "label"=>"Isi Article",
            "placeholder"=>"Masukan Article "
          )
        );

  }
	public function index() {
    //breadcump
    $data["title"]="Data Master Article";
    $data["deskripsi"]="Pengelolaan Data Master Article";
    $data["table_tampilan"]=$this->model_design->buat_table_dan_isinya($this->tabel,'id_article',$this->kolom,$this->action,$this->base,$this->tabel);
    $this->template->load($this->template_page, 'master_data/master/index_view', $data);
  }
  
  public function tambah_article(){
    $this->base="".base_url()."backend/article";

    $data["title"]="Tambah Data Master Article";
    $data["deskripsi"]="Silahkan isi form dibawah ini";
    $data["form_tambah"]=$this->model_design->buat_form_tambah($this->tabel,$this->form,$this->base);
    $this->template->load($this->template_page, 'master_data/master/tambah_view', $data);
  }


  public function edit_article($id){
    $data["title"]="Edit Data Master Article";
    $data["deskripsi"]="Silahkan isi form dibawah ini";
    $this->base = "".base_url()."backend/article/";
    $data["form_edit"]=$this->model_design->buat_form_edit($this->tabel,$this->form,$this->base,$id,'id_article');
    $this->template->load($this->template_page, 'master_data/master/edit_view', $data);
  }

  
}