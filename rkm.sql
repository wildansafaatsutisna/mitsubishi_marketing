-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 10.1.19-MariaDB - mariadb.org binary distribution
-- OS Server:                    Win32
-- HeidiSQL Versi:               9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Membuang struktur basisdata untuk rkm_bbl
CREATE DATABASE IF NOT EXISTS `rkm_bbl` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rkm_bbl`;

-- membuang struktur untuk table rkm_bbl.5r
CREATE TABLE IF NOT EXISTS `5r` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `dept` varchar(50) NOT NULL,
  `period` varchar(25) NOT NULL,
  `point` varchar(25) NOT NULL,
  `ringkas` varchar(25) NOT NULL,
  `rapi` varchar(25) NOT NULL,
  `resik` varchar(25) NOT NULL,
  `rawat` varchar(25) NOT NULL,
  `rajin` varchar(25) NOT NULL,
  `kategori` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel Program 5 R';

-- Membuang data untuk tabel rkm_bbl.5r: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `5r` DISABLE KEYS */;
/*!40000 ALTER TABLE `5r` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.dept_list
CREATE TABLE IF NOT EXISTS `dept_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dept` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel rkm_bbl.dept_list: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `dept_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `dept_list` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.fa_payment
CREATE TABLE IF NOT EXISTS `fa_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `periode` varchar(25) NOT NULL,
  `btbg_ba` varchar(25) NOT NULL,
  `sudah_proses` varchar(5) NOT NULL,
  `dalam_proses` varchar(5) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel FA Payment';

-- Membuang data untuk tabel rkm_bbl.fa_payment: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `fa_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `fa_payment` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.ga_fru
CREATE TABLE IF NOT EXISTS `ga_fru` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `ptcl_aset` varchar(5) NOT NULL,
  `case_open` varchar(5) NOT NULL,
  `case_close` varchar(5) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel GA FRU';

-- Membuang data untuk tabel rkm_bbl.ga_fru: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `ga_fru` DISABLE KEYS */;
/*!40000 ALTER TABLE `ga_fru` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.ga_koe
CREATE TABLE IF NOT EXISTS `ga_koe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `no_koe` varchar(25) NOT NULL,
  `qty` varchar(5) NOT NULL,
  `verifikasi` int(11) NOT NULL,
  `bpp_ga` varchar(25) NOT NULL,
  `bpp_ict` varchar(25) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel GA KOE';

-- Membuang data untuk tabel rkm_bbl.ga_koe: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `ga_koe` DISABLE KEYS */;
/*!40000 ALTER TABLE `ga_koe` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.hazoc
CREATE TABLE IF NOT EXISTS `hazoc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `dept` varchar(50) NOT NULL,
  `card` varchar(25) NOT NULL,
  `open` varchar(25) NOT NULL,
  `close` varchar(25) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Tabel Hazoc';

-- Membuang data untuk tabel rkm_bbl.hazoc: ~1 rows (lebih kurang)
/*!40000 ALTER TABLE `hazoc` DISABLE KEYS */;
INSERT INTO `hazoc` (`id`, `no_rkm`, `dept`, `card`, `open`, `close`, `log_by`) VALUES
	(1, '123131', '', 'wildan', '', '', '');
/*!40000 ALTER TABLE `hazoc` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.hrd_recruit
CREATE TABLE IF NOT EXISTS `hrd_recruit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `site` varchar(25) NOT NULL,
  `qty` varchar(5) NOT NULL,
  `mpp` varchar(5) NOT NULL,
  `progress` varchar(5) NOT NULL,
  `remark` varchar(5) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel HRD Recruitment';

-- Membuang data untuk tabel rkm_bbl.hrd_recruit: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `hrd_recruit` DISABLE KEYS */;
/*!40000 ALTER TABLE `hrd_recruit` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.hrd_training
CREATE TABLE IF NOT EXISTS `hrd_training` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `schedule` varchar(50) NOT NULL,
  `participant` varchar(50) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel HRD Training';

-- Membuang data untuk tabel rkm_bbl.hrd_training: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `hrd_training` DISABLE KEYS */;
/*!40000 ALTER TABLE `hrd_training` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.ict_antivirus
CREATE TABLE IF NOT EXISTS `ict_antivirus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `periode` varchar(25) NOT NULL,
  `uptodate` varchar(5) NOT NULL,
  `less_week` varchar(5) NOT NULL,
  `over_week` varchar(5) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel ICT Antivirus';

-- Membuang data untuk tabel rkm_bbl.ict_antivirus: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `ict_antivirus` DISABLE KEYS */;
/*!40000 ALTER TABLE `ict_antivirus` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.ict_helpdesk
CREATE TABLE IF NOT EXISTS `ict_helpdesk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `periode` varchar(25) NOT NULL,
  `ticket` varchar(5) NOT NULL,
  `close` varchar(5) NOT NULL,
  `open` varchar(5) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel ICT Helpdesk';

-- Membuang data untuk tabel rkm_bbl.ict_helpdesk: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `ict_helpdesk` DISABLE KEYS */;
/*!40000 ALTER TABLE `ict_helpdesk` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.master_notulen
CREATE TABLE IF NOT EXISTS `master_notulen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `dokumen` varchar(255) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel Master Notulen';

-- Membuang data untuk tabel rkm_bbl.master_notulen: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `master_notulen` DISABLE KEYS */;
/*!40000 ALTER TABLE `master_notulen` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.mkt_tunggakan
CREATE TABLE IF NOT EXISTS `mkt_tunggakan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `periode` varchar(25) NOT NULL,
  `sp1` varchar(25) NOT NULL,
  `sp2` varchar(25) NOT NULL,
  `sp3` varchar(25) NOT NULL,
  `pemadaman` varchar(25) NOT NULL,
  `nyala` varchar(25) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel Marketing Tunggakan';

-- Membuang data untuk tabel rkm_bbl.mkt_tunggakan: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `mkt_tunggakan` DISABLE KEYS */;
/*!40000 ALTER TABLE `mkt_tunggakan` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.mtc_workorder
CREATE TABLE IF NOT EXISTS `mtc_workorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `jenis_wo` varchar(25) NOT NULL,
  `target` varchar(25) NOT NULL,
  `finish` varchar(25) NOT NULL,
  `un_finish` varchar(25) NOT NULL,
  `in_progress` varchar(25) NOT NULL,
  `on_hold` varchar(25) NOT NULL,
  `overdue` varchar(25) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel MTC Workorders';

-- Membuang data untuk tabel rkm_bbl.mtc_workorder: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `mtc_workorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `mtc_workorder` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.pch_bpp
CREATE TABLE IF NOT EXISTS `pch_bpp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `periode` varchar(25) NOT NULL,
  `terima` varchar(25) NOT NULL,
  `selesai` varchar(25) NOT NULL,
  `in_proses` varchar(25) NOT NULL,
  `po_terbit` varchar(25) NOT NULL,
  `evaluasi_supplier` varchar(25) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel PCH Bpp';

-- Membuang data untuk tabel rkm_bbl.pch_bpp: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `pch_bpp` DISABLE KEYS */;
/*!40000 ALTER TABLE `pch_bpp` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.qcc_pss
CREATE TABLE IF NOT EXISTS `qcc_pss` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `dept` varchar(50) NOT NULL,
  `periode` varchar(50) NOT NULL,
  `tahap_1` varchar(5) NOT NULL,
  `tahap_2` varchar(5) NOT NULL,
  `tahap_3` varchar(5) NOT NULL,
  `tahap_4` varchar(5) NOT NULL,
  `selesai` varchar(5) NOT NULL,
  `total` varchar(5) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel QCC PSS';

-- Membuang data untuk tabel rkm_bbl.qcc_pss: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `qcc_pss` DISABLE KEYS */;
/*!40000 ALTER TABLE `qcc_pss` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.qcc_tulta
CREATE TABLE IF NOT EXISTS `qcc_tulta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `dept` varchar(50) NOT NULL,
  `periode` varchar(50) NOT NULL,
  `langkah_1` varchar(5) NOT NULL,
  `langkah_2` varchar(5) NOT NULL,
  `langkah_3` varchar(5) NOT NULL,
  `langkah_4` varchar(5) NOT NULL,
  `langkah_5` varchar(5) NOT NULL,
  `langkah_6` varchar(5) NOT NULL,
  `langkah_7` varchar(5) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  `selesai` varchar(5) NOT NULL,
  `total` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel QCC PSS';

-- Membuang data untuk tabel rkm_bbl.qcc_tulta: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `qcc_tulta` DISABLE KEYS */;
/*!40000 ALTER TABLE `qcc_tulta` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.rkm
CREATE TABLE IF NOT EXISTS `rkm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `periode` varchar(50) NOT NULL,
  `jam` datetime(6) NOT NULL,
  `tempat` varchar(255) NOT NULL,
  `status` varchar(5) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Tabel Master Data';

-- Membuang data untuk tabel rkm_bbl.rkm: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `rkm` DISABLE KEYS */;
/*!40000 ALTER TABLE `rkm` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.temuan_5r
CREATE TABLE IF NOT EXISTS `temuan_5r` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `dept` varchar(50) NOT NULL,
  `period` varchar(25) NOT NULL,
  `point` varchar(25) NOT NULL,
  `ringkas` varchar(25) NOT NULL,
  `rapi` varchar(25) NOT NULL,
  `resik` varchar(25) NOT NULL,
  `rawat` varchar(25) NOT NULL,
  `rajin` varchar(25) NOT NULL,
  `total` varchar(25) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel Program 5 R';

-- Membuang data untuk tabel rkm_bbl.temuan_5r: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `temuan_5r` DISABLE KEYS */;
/*!40000 ALTER TABLE `temuan_5r` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
