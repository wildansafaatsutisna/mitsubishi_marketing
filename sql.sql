-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 10.1.19-MariaDB - mariadb.org binary distribution
-- OS Server:                    Win32
-- HeidiSQL Versi:               9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Membuang struktur basisdata untuk rkm_bbl
CREATE DATABASE IF NOT EXISTS `rkm_bbl` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rkm_bbl`;

-- membuang struktur untuk table rkm_bbl.activity_dept
CREATE TABLE IF NOT EXISTS `activity_dept` (
  `id` int(11) DEFAULT NULL,
  `no_rkm` int(11) DEFAULT NULL,
  `dept` int(11) DEFAULT NULL,
  `activity` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `remark` text,
  `log_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel rkm_bbl.activity_dept: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `activity_dept` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity_dept` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.activity_list
CREATE TABLE IF NOT EXISTS `activity_list` (
  `id` int(11) DEFAULT NULL,
  `activity` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel rkm_bbl.activity_list: ~4 rows (lebih kurang)
/*!40000 ALTER TABLE `activity_list` DISABLE KEYS */;
INSERT INTO `activity_list` (`id`, `activity`) VALUES
	(NULL, 'System & Support'),
	(NULL, 'Document Control	'),
	(NULL, 'MtcC: Pengecatan dan perbaikan Gardu, week-38 : se'),
	(NULL, 'Legal');
/*!40000 ALTER TABLE `activity_list` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.dept_list
CREATE TABLE IF NOT EXISTS `dept_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dept` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel rkm_bbl.dept_list: ~3 rows (lebih kurang)
/*!40000 ALTER TABLE `dept_list` DISABLE KEYS */;
INSERT INTO `dept_list` (`id`, `dept`) VALUES
	(1, 'ICT'),
	(2, 'Main Plant'),
	(3, 'CHP');
/*!40000 ALTER TABLE `dept_list` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.fa_payment
CREATE TABLE IF NOT EXISTS `fa_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `periode` varchar(25) NOT NULL,
  `btbg_ba` varchar(25) NOT NULL,
  `sudah_proses` varchar(5) NOT NULL,
  `dalam_proses` varchar(5) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel FA Payment';

-- Membuang data untuk tabel rkm_bbl.fa_payment: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `fa_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `fa_payment` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.ga_fru
CREATE TABLE IF NOT EXISTS `ga_fru` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `ptcl_aset` varchar(5) NOT NULL,
  `case_open` varchar(5) NOT NULL,
  `case_close` varchar(5) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Tabel GA FRU';

-- Membuang data untuk tabel rkm_bbl.ga_fru: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `ga_fru` DISABLE KEYS */;
/*!40000 ALTER TABLE `ga_fru` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.ga_koe
CREATE TABLE IF NOT EXISTS `ga_koe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `no_koe` varchar(25) NOT NULL,
  `qty` varchar(5) NOT NULL,
  `verifikasi` int(11) NOT NULL,
  `bpp_ga` varchar(25) NOT NULL,
  `bpp_ict` varchar(25) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Tabel GA KOE';

-- Membuang data untuk tabel rkm_bbl.ga_koe: ~1 rows (lebih kurang)
/*!40000 ALTER TABLE `ga_koe` DISABLE KEYS */;
INSERT INTO `ga_koe` (`id`, `no_rkm`, `no_koe`, `qty`, `verifikasi`, `bpp_ga`, `bpp_ict`, `log_by`) VALUES
	(1, '1231', '123123', '1', 1, 'a', 'a', '');
/*!40000 ALTER TABLE `ga_koe` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.hazoc
CREATE TABLE IF NOT EXISTS `hazoc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `dept` varchar(50) NOT NULL,
  `card` varchar(25) NOT NULL,
  `open` varchar(25) NOT NULL,
  `close` varchar(25) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Tabel Hazoc';

-- Membuang data untuk tabel rkm_bbl.hazoc: ~1 rows (lebih kurang)
/*!40000 ALTER TABLE `hazoc` DISABLE KEYS */;
INSERT INTO `hazoc` (`id`, `no_rkm`, `dept`, `card`, `open`, `close`, `log_by`) VALUES
	(2, '1231', '2', 'card 1', 'open', 'close', '');
/*!40000 ALTER TABLE `hazoc` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.hrd_recruit
CREATE TABLE IF NOT EXISTS `hrd_recruit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `site` varchar(25) NOT NULL,
  `qty` varchar(5) NOT NULL,
  `mpp` varchar(5) NOT NULL,
  `progress` varchar(5) NOT NULL,
  `remark` varchar(5) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel HRD Recruitment';

-- Membuang data untuk tabel rkm_bbl.hrd_recruit: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `hrd_recruit` DISABLE KEYS */;
/*!40000 ALTER TABLE `hrd_recruit` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.hrd_training
CREATE TABLE IF NOT EXISTS `hrd_training` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `schedule` varchar(50) NOT NULL,
  `participant` varchar(50) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Tabel HRD Training';

-- Membuang data untuk tabel rkm_bbl.hrd_training: ~1 rows (lebih kurang)
/*!40000 ALTER TABLE `hrd_training` DISABLE KEYS */;
INSERT INTO `hrd_training` (`id`, `no_rkm`, `subject`, `schedule`, `participant`, `remark`, `log_by`) VALUES
	(1, '123', 'asd', 'ds', 'sda', 'sda', '');
/*!40000 ALTER TABLE `hrd_training` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.ict_antivirus
CREATE TABLE IF NOT EXISTS `ict_antivirus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `periode` varchar(25) NOT NULL,
  `uptodate` varchar(5) NOT NULL,
  `less_week` varchar(5) NOT NULL,
  `over_week` varchar(5) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel ICT Antivirus';

-- Membuang data untuk tabel rkm_bbl.ict_antivirus: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `ict_antivirus` DISABLE KEYS */;
/*!40000 ALTER TABLE `ict_antivirus` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.ict_helpdesk
CREATE TABLE IF NOT EXISTS `ict_helpdesk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `periode` varchar(25) NOT NULL,
  `ticket` varchar(5) NOT NULL,
  `close` varchar(5) NOT NULL,
  `open` varchar(5) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel ICT Helpdesk';

-- Membuang data untuk tabel rkm_bbl.ict_helpdesk: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `ict_helpdesk` DISABLE KEYS */;
/*!40000 ALTER TABLE `ict_helpdesk` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.master_notulen
CREATE TABLE IF NOT EXISTS `master_notulen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `dokumen` varchar(255) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel Master Notulen';

-- Membuang data untuk tabel rkm_bbl.master_notulen: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `master_notulen` DISABLE KEYS */;
/*!40000 ALTER TABLE `master_notulen` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.mkt_tunggakan
CREATE TABLE IF NOT EXISTS `mkt_tunggakan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `periode` varchar(25) NOT NULL,
  `sp1` varchar(25) NOT NULL,
  `sp2` varchar(25) NOT NULL,
  `sp3` varchar(25) NOT NULL,
  `pemadaman` varchar(25) NOT NULL,
  `nyala` varchar(25) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Tabel Marketing Tunggakan';

-- Membuang data untuk tabel rkm_bbl.mkt_tunggakan: ~1 rows (lebih kurang)
/*!40000 ALTER TABLE `mkt_tunggakan` DISABLE KEYS */;
INSERT INTO `mkt_tunggakan` (`id`, `no_rkm`, `periode`, `sp1`, `sp2`, `sp3`, `pemadaman`, `nyala`, `log_by`) VALUES
	(1, '1231', '123', '1', '2', '3', '231', '23', '');
/*!40000 ALTER TABLE `mkt_tunggakan` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.mtc_workorder
CREATE TABLE IF NOT EXISTS `mtc_workorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `jenis_wo` varchar(25) NOT NULL,
  `target` varchar(25) NOT NULL,
  `finish` varchar(25) NOT NULL,
  `un_finish` varchar(25) NOT NULL,
  `in_progress` varchar(25) NOT NULL,
  `on_hold` varchar(25) NOT NULL,
  `overdue` varchar(25) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel MTC Workorders';

-- Membuang data untuk tabel rkm_bbl.mtc_workorder: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `mtc_workorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `mtc_workorder` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.pch_bpp
CREATE TABLE IF NOT EXISTS `pch_bpp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `periode` varchar(25) NOT NULL,
  `terima` varchar(25) NOT NULL,
  `selesai` varchar(25) NOT NULL,
  `in_proses` varchar(25) NOT NULL,
  `po_terbit` varchar(25) NOT NULL,
  `evaluasi_supplier` varchar(25) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel PCH Bpp';

-- Membuang data untuk tabel rkm_bbl.pch_bpp: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `pch_bpp` DISABLE KEYS */;
/*!40000 ALTER TABLE `pch_bpp` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.program_5r
CREATE TABLE IF NOT EXISTS `program_5r` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `dept` varchar(50) NOT NULL,
  `periode` varchar(25) NOT NULL,
  `point` varchar(25) NOT NULL,
  `ringkas` varchar(25) NOT NULL,
  `rapi` varchar(25) NOT NULL,
  `resik` varchar(25) NOT NULL,
  `rawat` varchar(25) NOT NULL,
  `rajin` varchar(25) NOT NULL,
  `kategori` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Tabel Program 5 R';

-- Membuang data untuk tabel rkm_bbl.program_5r: ~1 rows (lebih kurang)
/*!40000 ALTER TABLE `program_5r` DISABLE KEYS */;
INSERT INTO `program_5r` (`id`, `no_rkm`, `dept`, `periode`, `point`, `ringkas`, `rapi`, `resik`, `rawat`, `rajin`, `kategori`) VALUES
	(1, '1231', '1', '321', '231', '123', '213', '123', '1', '231', '123');
/*!40000 ALTER TABLE `program_5r` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.qcc_pss
CREATE TABLE IF NOT EXISTS `qcc_pss` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `dept` varchar(50) NOT NULL,
  `periode` varchar(50) NOT NULL,
  `tahap_1` varchar(5) NOT NULL,
  `tahap_2` varchar(5) NOT NULL,
  `tahap_3` varchar(5) NOT NULL,
  `tahap_4` varchar(5) NOT NULL,
  `selesai` varchar(5) NOT NULL,
  `total` varchar(5) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Tabel QCC PSS';

-- Membuang data untuk tabel rkm_bbl.qcc_pss: ~1 rows (lebih kurang)
/*!40000 ALTER TABLE `qcc_pss` DISABLE KEYS */;
INSERT INTO `qcc_pss` (`id`, `no_rkm`, `dept`, `periode`, `tahap_1`, `tahap_2`, `tahap_3`, `tahap_4`, `selesai`, `total`, `log_by`) VALUES
	(1, '1231', '1', 'periode', '1', '2', '3', '4', 'seles', '', '');
/*!40000 ALTER TABLE `qcc_pss` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.qcc_tulta
CREATE TABLE IF NOT EXISTS `qcc_tulta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `dept` varchar(50) NOT NULL,
  `periode` varchar(50) NOT NULL,
  `langkah_1` varchar(5) NOT NULL,
  `langkah_2` varchar(5) NOT NULL,
  `langkah_3` varchar(5) NOT NULL,
  `langkah_4` varchar(5) NOT NULL,
  `langkah_5` varchar(5) NOT NULL,
  `langkah_6` varchar(5) NOT NULL,
  `langkah_7` varchar(5) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  `selesai` varchar(5) NOT NULL,
  `total` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Tabel QCC PSS';

-- Membuang data untuk tabel rkm_bbl.qcc_tulta: ~1 rows (lebih kurang)
/*!40000 ALTER TABLE `qcc_tulta` DISABLE KEYS */;
INSERT INTO `qcc_tulta` (`id`, `no_rkm`, `dept`, `periode`, `langkah_1`, `langkah_2`, `langkah_3`, `langkah_4`, `langkah_5`, `langkah_6`, `langkah_7`, `log_by`, `selesai`, `total`) VALUES
	(1, '123', '1', '20131', '1', '2', '3', '4', '5', '6', '7', '', 's', '');
/*!40000 ALTER TABLE `qcc_tulta` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.rkm
CREATE TABLE IF NOT EXISTS `rkm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `periode` varchar(50) NOT NULL,
  `jam` datetime(6) NOT NULL,
  `tempat` varchar(255) NOT NULL,
  `status` varchar(5) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Tabel Master Data';

-- Membuang data untuk tabel rkm_bbl.rkm: ~2 rows (lebih kurang)
/*!40000 ALTER TABLE `rkm` DISABLE KEYS */;
INSERT INTO `rkm` (`id`, `no_rkm`, `periode`, `jam`, `tempat`, `status`, `log_by`) VALUES
	(2, '123', '312', '0000-00-00 00:00:00.000000', '123', '', ''),
	(3, '1231', 'asd', '0000-00-00 00:00:00.000000', 'asd', '', '');
/*!40000 ALTER TABLE `rkm` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.rkm_notulen
CREATE TABLE IF NOT EXISTS `rkm_notulen` (
  `id` int(11) DEFAULT NULL,
  `no_rkm` varchar(50) DEFAULT NULL,
  `no_rkm_notulen` varchar(50) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `log_by` int(11) DEFAULT NULL,
  `nama_document` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel rkm_bbl.rkm_notulen: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `rkm_notulen` DISABLE KEYS */;
/*!40000 ALTER TABLE `rkm_notulen` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.temuan_5r
CREATE TABLE IF NOT EXISTS `temuan_5r` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_rkm` varchar(25) NOT NULL,
  `dept` varchar(50) NOT NULL,
  `periode` varchar(25) NOT NULL,
  `point` varchar(25) NOT NULL,
  `ringkas` varchar(25) NOT NULL,
  `rapi` varchar(25) NOT NULL,
  `resik` varchar(25) NOT NULL,
  `rawat` varchar(25) NOT NULL,
  `rajin` varchar(25) NOT NULL,
  `total` varchar(25) NOT NULL,
  `log_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Tabel Program 5 R';

-- Membuang data untuk tabel rkm_bbl.temuan_5r: ~1 rows (lebih kurang)
/*!40000 ALTER TABLE `temuan_5r` DISABLE KEYS */;
INSERT INTO `temuan_5r` (`id`, `no_rkm`, `dept`, `periode`, `point`, `ringkas`, `rapi`, `resik`, `rawat`, `rajin`, `total`, `log_by`) VALUES
	(1, '1231', '1', '2017', '23', '312', '123', '123', '123', '231', '132', '');
/*!40000 ALTER TABLE `temuan_5r` ENABLE KEYS */;

-- membuang struktur untuk table rkm_bbl.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel rkm_bbl.users: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`) VALUES
	(1, 'wildan.sutisna', 'ac174e67e662c4d31dcc2e8b16358024');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- membuang struktur untuk view rkm_bbl.view_activity_dept
-- Membuat tabel sementara untuk menangani kesalahan ketergantungan VIEW
CREATE TABLE `view_activity_dept` (
	`id` INT(11) NULL,
	`no_rkm` INT(11) NULL,
	`dept` INT(11) NULL,
	`activity` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`status` INT(11) NULL,
	`remark` TEXT NULL COLLATE 'latin1_swedish_ci',
	`log_by` INT(11) NULL,
	`departement` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- membuang struktur untuk view rkm_bbl.view_fa_payment
-- Membuat tabel sementara untuk menangani kesalahan ketergantungan VIEW
CREATE TABLE `view_fa_payment` (
	`id` INT(11) NOT NULL,
	`no_rkm` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`periode` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`btbg_ba` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`sudah_proses` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`dalam_proses` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`log_by` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- membuang struktur untuk view rkm_bbl.view_ga_fru
-- Membuat tabel sementara untuk menangani kesalahan ketergantungan VIEW
CREATE TABLE `view_ga_fru` (
	`id` INT(11) NOT NULL,
	`no_rkm` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`ptcl_aset` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`case_open` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`case_close` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`log_by` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- membuang struktur untuk view rkm_bbl.view_ga_koe
-- Membuat tabel sementara untuk menangani kesalahan ketergantungan VIEW
CREATE TABLE `view_ga_koe` (
	`id` INT(11) NOT NULL,
	`no_rkm` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`no_koe` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`qty` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`verifikasi` INT(11) NOT NULL,
	`bpp_ga` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`bpp_ict` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`log_by` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- membuang struktur untuk view rkm_bbl.view_hazoc
-- Membuat tabel sementara untuk menangani kesalahan ketergantungan VIEW
CREATE TABLE `view_hazoc` (
	`id` INT(11) NOT NULL,
	`no_rkm` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`dept` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`card` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`open` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`close` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`log_by` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`departement` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- membuang struktur untuk view rkm_bbl.view_hrd_recruit
-- Membuat tabel sementara untuk menangani kesalahan ketergantungan VIEW
CREATE TABLE `view_hrd_recruit` (
	`id` INT(11) NOT NULL,
	`no_rkm` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`site` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`qty` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`mpp` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`progress` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`remark` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`log_by` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- membuang struktur untuk view rkm_bbl.view_hrd_training
-- Membuat tabel sementara untuk menangani kesalahan ketergantungan VIEW
CREATE TABLE `view_hrd_training` (
	`id` INT(11) NOT NULL,
	`no_rkm` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`subject` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`schedule` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`participant` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`remark` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`log_by` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- membuang struktur untuk view rkm_bbl.view_ict_antivirus
-- Membuat tabel sementara untuk menangani kesalahan ketergantungan VIEW
CREATE TABLE `view_ict_antivirus` (
	`id` INT(11) NOT NULL,
	`no_rkm` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`periode` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`uptodate` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`less_week` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`over_week` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`log_by` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- membuang struktur untuk view rkm_bbl.view_mkt_tunggakan
-- Membuat tabel sementara untuk menangani kesalahan ketergantungan VIEW
CREATE TABLE `view_mkt_tunggakan` (
	`id` INT(11) NOT NULL,
	`no_rkm` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`periode` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`sp1` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`sp2` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`sp3` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`pemadaman` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`nyala` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`log_by` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- membuang struktur untuk view rkm_bbl.view_pch_bpp
-- Membuat tabel sementara untuk menangani kesalahan ketergantungan VIEW
CREATE TABLE `view_pch_bpp` (
	`id` INT(11) NOT NULL,
	`no_rkm` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`periode` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`terima` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`selesai` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`in_proses` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`po_terbit` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`evaluasi_supplier` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`log_by` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- membuang struktur untuk view rkm_bbl.view_program_limar
-- Membuat tabel sementara untuk menangani kesalahan ketergantungan VIEW
CREATE TABLE `view_program_limar` (
	`id` INT(11) NOT NULL,
	`no_rkm` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`dept` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`periode` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`point` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`ringkas` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`rapi` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`resik` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`rawat` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`rajin` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`kategori` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`departement` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- membuang struktur untuk view rkm_bbl.view_qcc_pss
-- Membuat tabel sementara untuk menangani kesalahan ketergantungan VIEW
CREATE TABLE `view_qcc_pss` (
	`id` INT(11) NOT NULL,
	`no_rkm` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`dept` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`periode` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`tahap_1` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`tahap_2` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`tahap_3` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`tahap_4` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`selesai` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`total` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`log_by` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`departement` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- membuang struktur untuk view rkm_bbl.view_qcc_tulta
-- Membuat tabel sementara untuk menangani kesalahan ketergantungan VIEW
CREATE TABLE `view_qcc_tulta` (
	`id` INT(11) NOT NULL,
	`no_rkm` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`dept` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`periode` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`langkah_1` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`langkah_2` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`langkah_3` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`langkah_4` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`langkah_5` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`langkah_6` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`langkah_7` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`log_by` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`selesai` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`total` VARCHAR(5) NOT NULL COLLATE 'latin1_swedish_ci',
	`departement` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- membuang struktur untuk view rkm_bbl.view_rkm_notulen
-- Membuat tabel sementara untuk menangani kesalahan ketergantungan VIEW
CREATE TABLE `view_rkm_notulen` (
	`id` INT(11) NULL,
	`no_rkm` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`no_rkm_notulen` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`url` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci',
	`log_by` INT(11) NULL,
	`nama_document` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- membuang struktur untuk view rkm_bbl.view_temuan_lima_r
-- Membuat tabel sementara untuk menangani kesalahan ketergantungan VIEW
CREATE TABLE `view_temuan_lima_r` (
	`id` INT(11) NOT NULL,
	`no_rkm` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`dept` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`periode` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`point` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`ringkas` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`rapi` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`resik` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`rawat` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`rajin` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`total` VARCHAR(25) NOT NULL COLLATE 'latin1_swedish_ci',
	`log_by` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`departement` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- membuang struktur untuk view rkm_bbl.view_activity_dept
-- Menghapus tabel sementara dan menciptakan struktur VIEW terakhir
DROP TABLE IF EXISTS `view_activity_dept`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_activity_dept` AS SELECT activity_dept.*, dept_list.dept as departement from activity_dept 
LEFT OUTER JOIN dept_list ON dept_list.id = activity_dept.dept
LEFT OUTER JOIN rkm ON rkm.no_rkm = activity_dept.no_rkm ;

-- membuang struktur untuk view rkm_bbl.view_fa_payment
-- Menghapus tabel sementara dan menciptakan struktur VIEW terakhir
DROP TABLE IF EXISTS `view_fa_payment`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_fa_payment` AS SELECT fa_payment.*  from fa_payment 
LEFT OUTER JOIN rkm ON rkm.no_rkm = fa_payment.no_rkm ;

-- membuang struktur untuk view rkm_bbl.view_ga_fru
-- Menghapus tabel sementara dan menciptakan struktur VIEW terakhir
DROP TABLE IF EXISTS `view_ga_fru`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_ga_fru` AS SELECT ga_fru.*  from ga_fru 
LEFT OUTER JOIN rkm ON rkm.no_rkm = ga_fru.no_rkm ;

-- membuang struktur untuk view rkm_bbl.view_ga_koe
-- Menghapus tabel sementara dan menciptakan struktur VIEW terakhir
DROP TABLE IF EXISTS `view_ga_koe`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_ga_koe` AS SELECT ga_koe.*  from ga_koe 
LEFT OUTER JOIN rkm ON rkm.no_rkm = ga_koe.no_rkm ;

-- membuang struktur untuk view rkm_bbl.view_hazoc
-- Menghapus tabel sementara dan menciptakan struktur VIEW terakhir
DROP TABLE IF EXISTS `view_hazoc`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_hazoc` AS SELECT hazoc.*, dept_list.dept as departement from hazoc 
LEFT OUTER JOIN dept_list ON dept_list.id = hazoc.dept
LEFT OUTER JOIN rkm ON rkm.no_rkm = hazoc.no_rkm ;

-- membuang struktur untuk view rkm_bbl.view_hrd_recruit
-- Menghapus tabel sementara dan menciptakan struktur VIEW terakhir
DROP TABLE IF EXISTS `view_hrd_recruit`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_hrd_recruit` AS SELECT hrd_recruit.*  from hrd_recruit 
LEFT OUTER JOIN rkm ON rkm.no_rkm = hrd_recruit.no_rkm ;

-- membuang struktur untuk view rkm_bbl.view_hrd_training
-- Menghapus tabel sementara dan menciptakan struktur VIEW terakhir
DROP TABLE IF EXISTS `view_hrd_training`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_hrd_training` AS SELECT hrd_training.*  from hrd_training 
LEFT OUTER JOIN rkm ON rkm.no_rkm = hrd_training.no_rkm ;

-- membuang struktur untuk view rkm_bbl.view_ict_antivirus
-- Menghapus tabel sementara dan menciptakan struktur VIEW terakhir
DROP TABLE IF EXISTS `view_ict_antivirus`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_ict_antivirus` AS SELECT ict_antivirus.*  from ict_antivirus 
LEFT OUTER JOIN rkm ON rkm.no_rkm = ict_antivirus.no_rkm ;

-- membuang struktur untuk view rkm_bbl.view_mkt_tunggakan
-- Menghapus tabel sementara dan menciptakan struktur VIEW terakhir
DROP TABLE IF EXISTS `view_mkt_tunggakan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_mkt_tunggakan` AS SELECT mkt_tunggakan.*  from mkt_tunggakan 
LEFT OUTER JOIN rkm ON rkm.no_rkm = mkt_tunggakan.no_rkm ;

-- membuang struktur untuk view rkm_bbl.view_pch_bpp
-- Menghapus tabel sementara dan menciptakan struktur VIEW terakhir
DROP TABLE IF EXISTS `view_pch_bpp`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_pch_bpp` AS SELECT pch_bpp.*  from pch_bpp 
LEFT OUTER JOIN rkm ON rkm.no_rkm = pch_bpp.no_rkm ;

-- membuang struktur untuk view rkm_bbl.view_program_limar
-- Menghapus tabel sementara dan menciptakan struktur VIEW terakhir
DROP TABLE IF EXISTS `view_program_limar`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_program_limar` AS SELECT program_5r.*, dept_list.dept as departement from program_5r 
LEFT OUTER JOIN dept_list ON dept_list.id = program_5r.dept
LEFT OUTER JOIN rkm ON rkm.no_rkm = program_5r.no_rkm ;

-- membuang struktur untuk view rkm_bbl.view_qcc_pss
-- Menghapus tabel sementara dan menciptakan struktur VIEW terakhir
DROP TABLE IF EXISTS `view_qcc_pss`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_qcc_pss` AS SELECT qcc_pss.*, dept_list.dept as departement from qcc_pss 
LEFT OUTER JOIN dept_list ON dept_list.id = qcc_pss.dept
LEFT OUTER JOIN rkm ON rkm.no_rkm = qcc_pss.no_rkm ;

-- membuang struktur untuk view rkm_bbl.view_qcc_tulta
-- Menghapus tabel sementara dan menciptakan struktur VIEW terakhir
DROP TABLE IF EXISTS `view_qcc_tulta`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_qcc_tulta` AS SELECT qcc_tulta.*, dept_list.dept as departement from qcc_tulta 
LEFT OUTER JOIN dept_list ON dept_list.id = qcc_tulta.dept
LEFT OUTER JOIN rkm ON rkm.no_rkm = qcc_tulta.no_rkm ;

-- membuang struktur untuk view rkm_bbl.view_rkm_notulen
-- Menghapus tabel sementara dan menciptakan struktur VIEW terakhir
DROP TABLE IF EXISTS `view_rkm_notulen`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_rkm_notulen` AS SELECT rkm_notulen.*  from rkm_notulen 
LEFT OUTER JOIN rkm ON rkm.no_rkm = rkm_notulen.no_rkm ;

-- membuang struktur untuk view rkm_bbl.view_temuan_lima_r
-- Menghapus tabel sementara dan menciptakan struktur VIEW terakhir
DROP TABLE IF EXISTS `view_temuan_lima_r`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_temuan_lima_r` AS SELECT temuan_5r.*, dept_list.dept as departement from temuan_5r 
LEFT OUTER JOIN dept_list ON dept_list.id = temuan_5r.dept
LEFT OUTER JOIN rkm ON rkm.no_rkm = temuan_5r.no_rkm ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
